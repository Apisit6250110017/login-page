import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Login Page'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);



  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  late SharedPreferences prefs;
  String name = "";

  @override
  void initState() {
    super.initState();
    _dataLogin();
  }

  void _dataLogin() async {
    prefs = await SharedPreferences.getInstance();
    name = prefs.getString("Email") ?? '';
    setState(() {
      email.text = name;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 100,
                    ),
                    Image.network(
                      'https://bit.ly/3uSGsn8',
                      width: 200,
                    ),
                    SizedBox(
                      height: 60,
                    ),
                    textfield(
                      data: email,
                      n: 'Email',
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    textfield(
                      data: password,
                      n: 'Password',
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          ButtonTheme(
                            minWidth: 300.0,
                            height: 50.0,
                            child: RaisedButton(
                              onPressed: () => login(),
                              child: Text(
                                "Login",
                                style: TextStyle(
                                    fontSize: 24, color: Colors.white),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void login() async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString("Email", email.text.toString());

    print('Login Success');

    await Fluttertoast.showToast(
        msg: 'Login Success',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 32.0);
  }
}

class textfield extends StatelessWidget {
  const textfield({
    Key? key,
    required this.data,
    required this.n,
  }) : super(key: key);

  final TextEditingController data;
  final String n;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: data,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: n,
        hintText: 'กรุณากรอก $n',
      ),
    );
  }
}
